const Swal = require('sweetalert2');

module.exports = {
    init: function() {
        $('.cta').on('click', function(e){
            e.preventDefault();

            let target = $(this).data('target');
            if ($('#img-'+target).length < 1) {
                Swal.fire({
                    title: "Error!",
                    text: 'Element does not exists',
                    icon: 'error'
                })
                return;
            }

            $('.slideshow-item').removeClass('visible').addClass('invisible');
            $('#img-'+target).addClass('visible').removeClass('invisible');
        });
    }
}
