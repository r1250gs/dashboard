const settings = function(){
  this.root_path = __dirname;
  this.project_assets = __dirname + "/app/gulp/";
  this.public_dir = "public"

  this.settings = {
    "name": "kisphp demo - change this value",
    "root_path": this.root_path,
    "project_assets": this.project_assets,
    "public_dir": this.public_dir,

    "js": {
      "external": {
        "sources": [
          'node_modules/jquery/dist/jquery.min.js',
          'node_modules/bootstrap/dist/js/bootstrap.min.js',
        ],
        "output_filename": "external.js",
        "output_dirname": this.public_dir + "/js/",
      },
      "project": {
        "sources": [
          this.project_assets + '/assets/js/app.js',
        ],
        "output_filename": "app.js",
        "output_dirname": this.public_dir + "/js/",
      }
    },
    "css": {
      "external": {
        "sources": [
          'node_modules/bootstrap/dist/css/bootstrap.min.css',
        ],
        "output_filename": "external.css",
        "output_dirname": this.public_dir + "/css/",
      },
      "project": {
        "sources": [
          this.project_assets + '/assets/css/main.css'
        ],
        "output_filename": "app.css",
        "output_dirname": this.public_dir + "/css/",
      }
    },
    "files": {
      "fonts": {
        "sources": [
          'node_modules/bootstrap/fonts/*.*',
        ],
        "output_dirname": this.public_dir + "/fonts"
      },
      "images": {
        "sources": [
          this.project_assets + '/assets/images/**/*.*'
        ],
        "output_dirname": this.public_dir + "/images"
      }
    }
  };

  return this.settings;
};

module.exports = settings();
