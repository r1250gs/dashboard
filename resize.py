#!/usr/bin/env python3
import os

from PIL import Image

for root, dirs, files in os.walk('images'):
    for file in files:
        if '.jpg' not in file or root != 'images':
            continue
        print(f'{root}/{file}')
        image = Image.open(f'{root}/{file}')
        image.thumbnail((1294, 630))
        image.save(f'public/images/{file}')
