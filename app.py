import json
import sys

from yaml import Loader, load
# from flask import render_template, Flask

# app = Flask(__name__)

from jinja2 import Environment, PackageLoader, select_autoescape
env = Environment(
    loader=PackageLoader("app"),
    autoescape=select_autoescape()
)

template = env.get_template('layout.html')


def validate_structure(items: dict):
    data = {}
    registry = {}

    for item in items:
        data.update({
            item['id']: list(item['links'].values()) if 'links' in item and item['links'] is not None else []
        })
        registry.update({
            item['id']: item['links'] if 'links' in item else {}
        })
        if item['id'] in data[item['id']]:
            print(f'{item["id"]} has a connection to itself')
        if 'links' in item and item['links'] is not None \
                and len(list(set(list(item['links'].values())))) != len(list(item['links'].values())):
            print(f'You have a duplicate link_type for {item["id"]} -> {item["links"]}')

    for id in registry:
        if id in registry and registry[id] is None:
            print(f"{id} has no connections")
            continue
        main_id_connections = registry[id]
        if len(list(main_id_connections)) < 1:
            print(f'{id} has no connections')
            continue
        for link_type in main_id_connections:
            if link_type in ['l', 'b']:
                check_correct_link(registry, id, link_type)


def check_correct_link(registry, main_id, con_type):
    reverse = {
        'l': 'r',
        'r': 'l',
        't': 'b',
        'b': 't',
    }
    connection_id = registry[main_id][con_type]

    if connection_id not in registry:
        print(f'{connection_id} is a dead connection for {main_id}')
    else:
        if registry[connection_id] is None \
                or reverse[con_type] not in registry[connection_id] \
                or (main_id < connection_id and main_id != registry[connection_id][reverse[con_type]]):
            print(f'missing link from {connection_id} to [{reverse[con_type]}: {main_id}]')


def connection_missing_back(target, source, type):
    print(f'{target} missing: {type}: {source}')


if __name__ == '__main__':
    with open('stages.yaml') as fr:
        data = load(fr.read(), Loader=Loader)

        base_url = "/dashboard"
        if len(sys.argv) > 1 and sys.argv[1] == 'dev':
            base_url = ''

        output = template.render(data=data, base_url=base_url)

        with open('public/index.html', 'w') as fw:
            fw.write(output)

        validate_structure(data['images'])
