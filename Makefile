.PHONY: help install save build resize

.DEFAULT_GOAL := help

help: ## Display this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "[36m%-30s[0m %s\n", $$1, $$2}'

install: ## Install packages
	pip install -r requirements.txt

save: ## Export packages list
	pip freeze | sort > requirements.txt

build:
	docker build -f docker/Dockerfile -t images-resize .

run:
	docker run -v $(shell pwd):/app -w /app -it images-resize bash
